<?php
/**
 * @category Scandiweb
 * @package Scandiweb\Promo\Setup
 * @author Abror Abdullaev <info@scandiweb.com / abror@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Scandiweb\Promo\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->_installPromoTable($setup);
        $this->_installPromoStoreTable($setup);
        $this->_addForeignKeys($setup);
        $setup->endSetup();
    }

    /**
     * Create table "scandiweb_promo_block"
     *
     * @param SchemaSetupInterface $setup
     *
     * @throws \Zend_Db_Exception
     */
    protected function _installPromoTable(SchemaSetupInterface $setup)
    {
        $table = $setup->getConnection()
            ->newTable($setup->getTable('scandiweb_promo_block'))
            ->addColumn(
                'promo_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Promo ID'
            )
            ->addColumn(
                'identifier',
                Table::TYPE_TEXT,
                100,
                ['nullable' => false]
            )
            ->addColumn(
                'content',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Promo Title'
            )
            ->addColumn(
                'is_active',
                Table::TYPE_BOOLEAN,
                null,
                ['nullable' => false, 'default' => true],
                'Is Promo Active?'
            )
            ->addIndex(
                $setup->getIdxName('scandiweb_promo_block', ['identifier']),
                ['identifier']
            )
            ->addIndex(
                $setup->getIdxName('scandiweb_promo_block', ['promo_id']),
                ['promo_id']
            )
            ->setComment('Scandi Promo Blocks');

        $setup->getConnection()->createTable($table);
    }

    /**
     * Create table "scandiweb_promo_block_store"
     *
     * @param SchemaSetupInterface $setup
     */
    protected function _installPromoStoreTable(SchemaSetupInterface $setup)
    {
        $installer = $setup;
        /** @var $connection \Magento\Framework\DB\Adapter\Pdo\Mysql */
        $connection = $setup->getConnection();

        $table = $connection
            ->newTable(
                $installer->getTable('scandiweb_promo_block_store')
            )
            ->addColumn(
                'promo_id',
                Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'Promo ID'
            )
            ->addColumn(
                'store_id',
                Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'Store ID'
            )
            ->addIndex(
                $installer->getIdxName('scandiweb_promo_block_store', ['store_id']),
                ['store_id']
            )
            ->addIndex(
                $installer->getIdxName('scandiweb_promo_block_store', ['promo_id']),
                ['promo_id']
            )
            ->setComment('Promo Block To Store Linkage Table');

        $installer->getConnection()->createTable($table);
    }

    /**
     * apply all the necessary foreign keys
     *
     * @param SchemaSetupInterface $setup
     */
    protected function _addForeignKeys(SchemaSetupInterface $setup)
    {
        $installer = $setup;
        /** @var $connection \Magento\Framework\DB\Adapter\Pdo\Mysql */
        $connection = $setup->getConnection();

        /**
         * apply foreign keys for Menu-Store mapping
         */
        $connection->addForeignKey(
            $installer->getFkName(
                'scandiweb_promo_block_store',
                'promo_id',
                'scandiweb_promo_block',
                'promo_id'
            ),
            $installer->getTable('scandiweb_promo_block_store'),
            'promo_id',
            $installer->getTable('scandiweb_promo_block'),
            'promo_id',
            Table::ACTION_CASCADE
        );

        $connection->addForeignKey(
            $installer->getFkName(
                'scandiweb_promo_block_store',
                'store_id',
                'store',
                'store_id'
            ),
            $installer->getTable('scandiweb_promo_block_store'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            Table::ACTION_CASCADE
        );
    }
}
