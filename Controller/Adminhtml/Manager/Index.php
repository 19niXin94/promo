<?php
namespace Scandiweb\Promo\Controller\Adminhtml\Manager;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * @category Scandiweb
 * @package Scandiweb\Promo
 * @author Abror Abdullaev <info@scandiweb.com / abror@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *
 * Class Index
 */
class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Scandiweb_Promo::promo_navigation';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Scandiweb_Promo::promo_navigation');
        $resultPage->addBreadcrumb(__('Promo Manager'), __('Promo Manager'));
        $resultPage->addBreadcrumb(__('Manager Promo'), __('Manage Promo'));
        $resultPage->getConfig()->getTitle()->prepend(__('Promo Manager'));

        return $resultPage;
    }

    /*
	 * Check permission via ACL resource
	 */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Scandiweb_Promo::promo_navigation');
    }

    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Scandiweb_Promo::promo_navigation');
       $resultPage->getConfig()->getTitle()->prepend(__('Promo Manager'));

        //Add bread crumb
        $resultPage->addBreadcrumb(__('Promo Manager'), __('Promo Manager'));
        $resultPage->addBreadcrumb(__('Manager Promo'), __('Manage Promo'));

        return $this;
    }
}
