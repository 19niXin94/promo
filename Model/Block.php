<?php
/**
 * @category Scandiweb
 * @package Scandiweb\Promo\Setup
 * @author Abror Abdullaev <info@scandiweb.com / abror@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Scandiweb\Promo\Model;

use Scandiweb\Promo\Api\Data\PromoInterface;

/**
 * @category Scandiweb
 * @package Scandiweb\Menumanager\Model
 * @author Dmitrijs Sitovs <info@scandiweb.com / dmitrijssh@scandiweb.com / dsitovs@gmail.com>
 * @copyright Copyright (c) 2015 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *
 * Class Menu
 */
class Block extends \Magento\Framework\Model\AbstractModel implements PromoInterface
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'scandiweb_promo_block';

    /**
     * @var string
     */
    protected $_cacheTag = 'scandiweb_promo_block';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'scandiweb_promo_block';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Scandiweb\Promo\Model\ResourceModel\Block');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::PROMO_ID);
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getData(self::IDENTIFIER);
    }

    /**
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Scandiweb\Promo\Api\Data\PromoInterface
     */
    public function setId($id)
    {
        return $this->setData(self::PROMO_ID, $id);
    }

    /**
     * Set URL Key
     *
     * @param string $identifier
     * @return \Scandiweb\Promo\Api\Data\PromoInterface
     */
    public function setIdentifier($identifier)
    {
        return $this->setData(self::IDENTIFIER, $identifier);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     * @return \Scandiweb\Promo\Api\Data\PromoInterface
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }
}
