<?php
namespace Scandiweb\Promo\Api\Data;

/**
 * @category Scandiweb
 * @package Scandiweb\Menumanager\Api\Data
 * @author Dmitrijs Sitovs <info@scandiweb.com / dmitrijssh@scandiweb.com / dsitovs@gmail.com>
 * @copyright Copyright (c) 2015 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *
 * Interface MenuInterface
 */
interface PromoInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const PROMO_ID = 'promo_id';
    const IDENTIFIER = 'identifier';
    const CONTENT = 'content';
    const IS_ACTIVE = 'is_active';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Identifier
     *
     * @return string
     */
    public function getIdentifier();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Scandiweb\Promo\Api\Data\PromoInterface
     */
    public function setId($id);

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return \Scandiweb\Promo\Api\Data\PromoInterface
     */
    public function setIdentifier($identifier);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Scandiweb\Promo\Api\Data\PromoInterface
     */
    public function setIsActive($isActive);
}
