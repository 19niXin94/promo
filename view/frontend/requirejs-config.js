/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            promoBlock: 'Scandiweb_Promo/js/promoBlock'
        }
    }
};
