define([
        'jquery'
    ],
    function ($) {
        "use strict";

        $.widget('mage.scandiPromo', {
            $time: 4000,
            $defaultContent: true,

            options: {
                pageMaskColor: '#808080'
            },

            /**
             * Initialization of options
             * @private
             */
            _init: function () {
                if (this.options.time) {
                    this.$time = this.options.time * 1000;
                }

                if (this.options.content) {
                    this.$content = this.options.content;
                }

                //Show promo block
                this.showPromo();
            },

            /**
             * Main function that creates and manages the promo block
             */
            showPromo: function () {
                var promoMask = this._createMask(),
                    widget = this;

                setTimeout(function() {
                    document.body.appendChild(promoMask);
                    widget.showBlock();
                }, this.$time);
            },

            /**
             * Create a display mask
             * @returns {Element} div
             * @private
             */
            _createMask: function () {
                var promoPageMask = document.createElement('div');
                promoPageMask.id = "promo-page-mask";
                promoPageMask.style.backgroundColor = this.options.pageMaskColor;

                return promoPageMask;
            },

            /**
             * Insert a promo block into content
             * @public
             */
            showBlock: function () {
                if (this.$content) {
                    this.$content = this._wrapContent(this.$content);
                }

                var returnButton = this._createCloseButton();
                if (returnButton) {
                    this.$content.appendChild(returnButton);
                }

                document.body.appendChild(this.$content);
            },

            /**
             * Wrap the option.content with default wrapper
             * @param content
             * @returns {Element} div
             * @private
             */
            _wrapContent: function (content) {
                var wrappedContent = document.createElement('div');
                wrappedContent.className = 'defaultPromoWrapper';
                wrappedContent.innerHTML = content;

                return wrappedContent;
            },

            /**
             * Create close button for promo
             *
             * @requires jQuery
             * @returns close Button div
             * @private
             */
            _createCloseButton: function () {
                var closeBtn = $(".defaultPromoWrapper").find(".promo-btn-close");

                if(closeBtn.length = 0) {
                    /*
                     On Click close the promo and save a cookie
                     */
                    $(closeBtn).click(function() {
                        document.cookie = "promoBlockClosed=true";
                        $("#promo-page-mask").remove();
                        $(".defaultPromoWrapper").remove();
                    });

                    return closeBtn;
                }

                return null;
            }
        });

        return $.mage.scandiPromo;
    });