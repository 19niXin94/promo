<?php
namespace Scandiweb\Promo\Block;
/**
 * @category Scandiweb
 * @package Scandiweb\Promo
 * @author Abror Abdullaev <info@scandiweb.com / abror@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 *
 * Class Promo
 */
class Promo extends \Magento\Framework\View\Element\Template
{
    public function getContent() {
        $content = "
            <div>
                <h1>This is a test promo</h1>
                <span class='promo-btn-close'>X</span>
                <p>Don't forget to edit it</p>
            </div>
        ";

        return json_encode([$content]);
    }
}
