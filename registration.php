<?php
/**
 * @category Scandiweb
 * @package Scandiweb\Promo
 * @author Abror Abdullaev <info@scandiweb.com / abror@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Scandiweb_Promo',
    __DIR__
);
